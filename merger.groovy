#!/usr/bin/env groovy

class MergedFile {
    boolean isShell;
    Set<String> imports = new HashSet<>();
    StringBuilder content = new StringBuilder();
}
String USAGE = 'merger [-sh -f] <src_1.groovy, src_2.groovy, ..> <output_name>';

if (this.args.length == 0) {
    println(USAGE);
    System.exit(1);
}

int argIndex = 0;

boolean isShell = this.args[0].equals('-sh');
if (isShell) {
    argIndex++;
}

boolean isForced = this.args[argIndex].equals('-f');
if (isForced) {
    argIndex++;
}

if (this.args[argIndex..-1].size() < 3) {
    println(USAGE);
    System.exit(1);
}

String newFileName = this.args[-1];
File workingDir = new File(System.getProperty('user.dir'));
File newFile = new File(workingDir, newFileName);
if (newFile.exists() && !isForced) {
    println("ERROR: File '${newFile.absolutePath}' is already exists");
    println('TIP:   Change output file name or use -f option for rewrite file');
    System.exit(1);
}

MergedFile newFileObj = new MergedFile(isShell: isShell);
for (sourceName in args[argIndex..-2]) {
    if (!sourceName.endsWith('.groovy')) {
        println("ERROR: Wrong source file name ${sourceName}");
        println('TIP:   Source file name must ends with \'.groovy\'');
        System.exit(1);
    }
    File source = new File(workingDir, sourceName);
    if (!source.exists()) {
        println("ERROR: There is no such file '${source.absolutePath}");
        System.exit(1);
    }
    newFileObj.content
    .append('\n')
    .append("/* START '${sourceName}' FILE CONTENT */")
    .append('\n');
    source.eachLine {
            if (it.startsWith('import ')) {
                newFileObj.imports.add(it);
            } else if (it.startsWith('#!/')) {
                return;
            } else {
                newFileObj.content.append(it).append('\n');
            }
    };
    newFileObj.content
    .append('\n')
    .append("/* END '${sourceName}' FILE CONTENT */")
    .append('\n');
}

BufferedWriter writer = newFile.newWriter('UTF-8', false);
if (newFileObj.isShell) {
    writer.write('#!/usr/bin/env groovy\n\n');
}
newFileObj.imports.each {
    writer.write("${it}\n");
}
writer.write('\n\n');
writer.write(newFileObj.content.toString());
writer.close();